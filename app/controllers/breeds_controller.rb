class BreedsController < ApplicationController
  def by_bread
    @breed = DogBreedFetcher.fetch(params['breed'])
  end
end
