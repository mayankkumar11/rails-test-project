module BreedsHelper
  def get_bread
    JSON.parse(File.read('public/bread.json')).keys
  end
end
